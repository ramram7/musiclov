//
//  MLConstants.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 07.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

struct MLConstants {

	struct StoryboardName {
		static let WelcomeTutorial                  = "WelcomeTutorial"
	}

	struct StoryboardId {
		static let WelcomeViewController            = "MLTutorialViewController"
	}

	struct NibNames {
		static let EmptySearchView					= "MLEmptySearchView"
	}

	struct Segues {
		static let WelcomeTutorial					= "toWelcomeTutorial"
		static let AlbumArtistDetail				= "toAlbumArtistDetail"
		static let ArtistDetailViewController		= "toArtistDetailViewController"
	}

	struct UserDefaults {
		static let AppAlreadyLaunchedForFirstTime	= "MusicLovAlreadyLaunched"
	}

	struct PListKeys {
		static let discogsBaseURL					= "DiscogsBaseURL"
		static let consumerSecret					= "ConsumerSecret"
		static let consumerKey						= "ConsumerKey"
	}

	struct APIConstants {
		static let key								= "key"
		static let secret							= "secret"
		static let releaseTitle						= "release_title"
		static let results 							= "results"
		static let id 								= "id"
		static let thumb							= "thumb"
		static let country							= "country"
		static let uri								= "uri"
		static let year 							= "year"
		static let title							= "title"
		static let resourceURL						= "resource_url"
		static let format							= "format"
		static let genre							= "genre"
		static let style							= "style"
		static let images							= "images"
		static let tracklist						= "tracklist"
		static let duration							= "duration"
		static let position							= "position"
		static let perPage 							= "per_page"
		static let artists							= "artists"
		static let name								= "name"
	}

	struct CellReuseId {

		static let AlbumCell						= "MLAlbumTableViewCell"
		static let AlbumThumbnailCell				= "MLAlbumThumbTableViewCell"
		static let ArtistNameCell					= "MLArtistTableViewCell"
		static let AlbumDetailsCell					= "MLAlbumDetailsTableViewCell"
		static let AlbumTracksCell			 		= "MLTitleTableViewCell"
		static let TrackCell  						= "MLElementTableViewCell"
		static let ElementDescriptionCell			= "MLElementDescriptionTableViewCell"
	}
}
