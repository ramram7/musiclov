//
//  APIManager.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 08.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

struct APIManager {

	// ========================================================
	// MARK: - Static properties for APIManager
	// ========================================================

	static let defaultSession 	= URLSession(configuration: .default)
	static var dataTask			: URLSessionDataTask?


	// ========================================================
	// MARK: - Private Methods to make connection with API
	// ========================================================

	/// Method to send request with authentication
	///
	/// - Parameters:
	///   - parameters: parameters to be sent with the request
	///   - completion: completion returning response and error, if needed
	static func requestWithAuthentication(path: String, parameters: [String: Any], completion: ((_ response: [String: Any]?, _ error: Error?) -> Void)?) {

		self.dataTask?.cancel()

		// fetch information from PList about base url
		guard
			let pListDictionary = self.dictionaryFromPList(),
			let baseURLString = pListDictionary[MLConstants.PListKeys.discogsBaseURL] as? String,
			var urlComponents = URLComponents.init(string: baseURLString) else {
				completion?(nil, nil)
				return
		}

		urlComponents.path = path
		urlComponents = self.addParametersToURLComponents(urlComponents: &urlComponents, parameters: parameters)
		urlComponents = self.addAuthenticationToURLComponents(urlComponents: &urlComponents)

		guard let url = urlComponents.url else {
			completion?(nil, nil)
			return
		}

		self.dataTask = self.configureDataTaskForRequest(url: url, completion: completion)
		self.dataTask?.resume()
	}

	private static func configureDataTaskForRequest(url: URL, completion: ((_ response: [String: Any]?, _ error: Error?) -> Void)?) -> URLSessionDataTask? {

		return self.defaultSession.dataTask(with: url, completionHandler: { (data: Data?, urlResponse: URLResponse?, error: Error?) in

			guard let data = data else {
				self.handleErrorIfNecessary(error: error, completion: completion)
				return
			}

			var response: [String: Any]?

			do {
				response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
			} catch let parseError {
				completion?(nil, parseError)
			}

			completion?(response, nil)
		})
	}

	/// Method to add authentication to url Components
	///
	/// - Parameter urlComponents: url components from request
	/// - Returns: new url components with consumer key and consumer secret
	private static func addAuthenticationToURLComponents(urlComponents: inout URLComponents) -> URLComponents {

		guard
			let pListDictionary = self.dictionaryFromPList(),
			let consumerKey = pListDictionary[MLConstants.PListKeys.consumerKey] as? String,
			let consumerSecret = pListDictionary[MLConstants.PListKeys.consumerSecret] as? String else {
				return urlComponents
		}

		let consumerKeyQueryItem = URLQueryItem.init(name: MLConstants.APIConstants.key, value: consumerKey)
		let consumerSecretQueryItem = URLQueryItem.init(name: MLConstants.APIConstants.secret, value: consumerSecret)

		urlComponents.queryItems?.append(consumerKeyQueryItem)
		urlComponents.queryItems?.append(consumerSecretQueryItem)
		return urlComponents
	}

	/// Method to add parameters to url Components
	///
	/// - Parameters:
	///   - urlComponents: url components from request
	///   - parameters: parameters to be sent with the request
	/// - Returns: new url components with parameters
	private static func addParametersToURLComponents(urlComponents: inout URLComponents, parameters: [String: Any]) -> URLComponents {

		urlComponents.queryItems = [URLQueryItem]()

		for (key, value) in parameters {
			let queryItem = URLQueryItem(name: key, value: "\(value)")
			urlComponents.queryItems?.append(queryItem)
		}

		return urlComponents
	}

	private static func handleErrorIfNecessary(error: Error?, completion: ((_ response: [String: Any]?, _ error: Error?) -> Void)?) {
		// TODO: Handle error
	}

	private static func dictionaryFromPList() -> [String: AnyObject]? {

		guard
			let path = Bundle.main.path(forResource: "MusicLov-Info", ofType: "plist"),
			let dictionary = NSDictionary.init(contentsOfFile: path) as? [String: AnyObject] else {
				return nil
		}

		return dictionary
	}

	static func fetchImageResourceURLStringForElement(id: Int, path: String, completion: ((_ resourceURLString: String, _ error: Error?) -> Void)?) {

		self.requestWithAuthentication(path: "/\(path)/\(id)", parameters: [:]) { (response: [String: Any]?, error: Error?) in

			guard
				let albumImages = response?[MLConstants.APIConstants.images] as? [[String: Any]],
				let resourceURLString = albumImages.first?[MLConstants.APIConstants.resourceURL] as? String else {
					return
			}

			completion?(resourceURLString, nil)
		}
	}
}

