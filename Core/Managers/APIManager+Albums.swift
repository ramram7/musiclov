//
//  APIManager+Albums.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 12.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

extension APIManager {
	
	/// Method to fetch albums with search string
	///
	/// - Parameters:
	///   - title: text introduced by the user
	///   - completion: completion returning an array of albums and error, if needed
	static func fetchAlbums(title: String, completion: ((_ albums: [Album], _ error: Error?) -> Void)?) {

		// TODO: Add pagination
		let parameters = [MLConstants.APIConstants.releaseTitle: title, MLConstants.APIConstants.perPage: 100] as [String : Any]
		self.requestWithAuthentication(path: "/database/search", parameters: parameters) { (response: [String: Any]?, error: Error?) in
			guard let results = response?[MLConstants.APIConstants.results] as? [[String: Any]] else {
				return
			}

			let albums = self.albumsArrayFromArrayResults(results: results)
			completion?(albums, nil)
		}
	}

	static func fetchImageResourceURLStringFromAlbum(albumId: Int, completion: ((_ resourceURLString: String, _ error: Error?) -> Void)?) {

		self.fetchImageResourceURLStringForElement(id: albumId, path: "releases", completion: completion)
	}

	private static func albumsArrayFromArrayResults(results: [[String: Any]]) -> [Album] {

		var albums = [Album]()

		results.forEach({ (result: [String: Any]) in
			guard
				let id = result[MLConstants.APIConstants.id] as? Int,
				let thumb = result[MLConstants.APIConstants.thumb] as? String,
				let country = result[MLConstants.APIConstants.country] as? String,
				let uri = result[MLConstants.APIConstants.uri] as? String,
				let year = result[MLConstants.APIConstants.year] as? String,
				let title = result[MLConstants.APIConstants.title] as? String,
				let resourceURL = result[MLConstants.APIConstants.resourceURL] as? String,
				let format = result[MLConstants.APIConstants.format] as? [String],
				let genre = result[MLConstants.APIConstants.genre] as? [String],
				let style = result[MLConstants.APIConstants.style] as? [String] else {
					return
			}

			let album = Album(id: id, thumb: thumb, country: country, uri: uri, year: year, title: title, resourceURL: resourceURL, formats: format, genre: genre, style: style)
			albums.append(album)
		})

		return albums
	}

}
