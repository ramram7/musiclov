//
//  APIManager+Artists.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 12.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

extension APIManager {

	static func fetchImageResourceURLStringFromArtist(artistId: Int, completion: ((_ resourceURLString: String, _ error: Error?) -> Void)?) {

		self.fetchImageResourceURLStringForElement(id: artistId, path: "artists", completion: completion)
	}

	static func fetchTracksAndArtistForAlbum(with id: Int, completion: ((_ tracks: [Track]?, _ artist: Artist?, _ error: Error?) -> Void)?) {

		self.requestWithAuthentication(path: "/releases/\(id)", parameters: [:]) { (response: [String: Any]?, error: Error?) in

			guard let results = response?[MLConstants.APIConstants.tracklist] as? [[String: Any]] else {
				return
			}

			let artist = self.fetchArtistName(response: response)
			let tracks = self.tracksArrayFromArrayResults(results: results)

			completion?(tracks, artist, nil)
		}
	}

	static func fetchDescriptionAndMembers(with artistId: Int, completion: ((_ description: String?, _ members: [Member]?, _ error: Error?) -> Void)?) {

		self.requestWithAuthentication(path: "/artists/\(artistId)", parameters: [:]) { (response: [String: Any]?, error: Error?) in

			let artistDescription = self.parseAritstDescription(response: response)
			let members = self.parseMembersFromArtist(response: response)

			completion?(artistDescription, members, nil)
		}
	}

	private static func tracksArrayFromArrayResults(results: [[String: Any]]) -> [Track] {

		var tracks = [Track]()

		results.forEach({ (trackDictionary: [String: Any]) in

			guard
				let duration = trackDictionary[MLConstants.APIConstants.duration] as? String,
				let positionString = trackDictionary[MLConstants.APIConstants.position] as? String,
				let position = Int(positionString),
				let title = trackDictionary[MLConstants.APIConstants.title] as? String else {
					return
			}

			let track = Track(title: title, duration: duration, position: position)
			tracks.append(track)
		})

		return tracks
	}

	private static func parseAritstDescription(response: [String: Any]?) -> String? {
		guard let artistProfile = response?["profile"] as? String else {
			return nil
		}

		return artistProfile
	}

	private static func parseMembersFromArtist(response: [String: Any]?) -> [Member]? {
		guard let membersFromArtist = response?["members"] as? [[String: Any]] else {
			return nil
		}

		var members = [Member]()
		membersFromArtist.forEach { (memberDictionary: [String: Any]) in

			guard
				let name = memberDictionary["name"] as? String,
				let id = memberDictionary["id"] as? Int else {
					return
			}

			let member = Member(name: name, id: id)
			members.append(member)
		}

		return members
	}

	private static func fetchArtistName(response: [String: Any]?) -> Artist? {
		guard let artistDictionaryArray = response?[MLConstants.APIConstants.artists] as? [[String: Any]] else {
			return nil
		}

		guard
			let artistName = artistDictionaryArray.first?[MLConstants.APIConstants.name] as? String,
			let artistId = artistDictionaryArray.first?[MLConstants.APIConstants.id] as? Int else {
				return nil
		}

		return Artist(name: artistName, id: artistId)
	}
}
