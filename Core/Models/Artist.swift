//
//  Artist.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 11.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

struct Artist {

	let name	: String
	let id		: Int
}
