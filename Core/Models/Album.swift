//
//  Album.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 09.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

struct Album {

	let id 			: Int
	let thumb		: String
	let country		: String
	let uri			: String
	let year		: String
	let title		: String
	let resourceURL	: String
	let formats		: [String]
	let genre		: [String]
	let style		: [String]
}
