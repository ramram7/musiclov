//
//  Track.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 10.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import Foundation

struct Track {

	let title		: String
	let duration	: String
	let position	: Int
}
