//
//  MLOverviewViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 12.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLOverviewViewController				: UIViewController {

	@IBOutlet private weak var tableView	: UITableView?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.tableView?.tableFooterView = UIView()
	}
}

extension MLOverviewViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") else {
			return UITableViewCell()
		}

		cell.textLabel?.numberOfLines = 0
		cell.selectionStyle = .none
		cell.textLabel?.text = """
		This small project was created to give a solution to the code challenge from Neofonie

		The requirements were met:

		✅ As a user I want to search for an album by its title.
		✅ As a user I want to see the search result in a table view.
		✅ As a user I want to see a detailed screen for the selected album.
		✅ As a user I want to see an artist page for the album’s artist.

		As extra, the following list was also implemented:

		✅ Localization
		✅ Layout for all devices and orientations (as I used just native elements, with constraints implementation was enough to make this possible)
		✅ An extra Tutorial View in the first launch

		What was not possible to implement because time and diapers (my daughters, of course 😀):

		- Unit Test
		- Favorite list (with DB)
		- Search by artist

		I did some tests, yes, but I did not catch all the cases from responses coming from API (awesome API, I have to say)
		The following albums are my suggestions to test the happy path:

		♦️ In Schwarz -> Kraftklub
		♦️ Made in Heaven -> Queen
		♦️ Laune der Natur -> Die Toten Hosen

		Cheers and Saludos!!
		"""
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
}

