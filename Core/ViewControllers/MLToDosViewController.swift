//
//  MLToDosViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 12.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLToDosViewController					: UIViewController {

	@IBOutlet private weak var tableView	: UITableView?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.tableView?.tableFooterView = UIView()
	}
}

extension MLToDosViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") else {
			return UITableViewCell()
		}

		cell.textLabel?.numberOfLines = 0
		cell.selectionStyle = .none
		cell.textLabel?.text = """
		Some ToDos:

		♠️ The biggest ToDo is UnitTest. I focused on functionality, but tests are always something very important during development
		♠️ Pagination in search
		♠️ Better handling of errors
		♠️ Add activities to wait for server answers
		"""
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
}

