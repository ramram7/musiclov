//
//  MLArtistDetailViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 10.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

// ========================================================
// MARK - ArtistDetail Enum
//
// Enum including all artist detail cases
// Every case contains three variables:
//     * image - Thumbnail from album
//     * description - small profile from
//     * membersTitle
//     * members - members of the group
//
// ========================================================

enum ArtistDetail: Int {

	case image = 0
	case description
	case membersTitle
	case members

	static func allValues() -> [ArtistDetail] {
		return [.image, .description, .membersTitle, .members]
	}

	var cellHeight: CGFloat {
		switch self {
		case .image,
			 .description	: return 300
		case .membersTitle,
			 .members		: return 44
		}
	}

	func cellForAlbumDetail(tableView: UITableView, artistResourceURLString: String?, artistDescription: String?, member: Member?) -> UITableViewCell {
		switch self {
		case .image:

			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.AlbumThumbnailCell) as? MLAlbumThumbTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(resourceURLString: artistResourceURLString)
			return cell
		case .description:

			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.ElementDescriptionCell) as? MLElementDescriptionTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(description: artistDescription)
			return cell
		case .membersTitle:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.AlbumTracksCell) as? MLTitleTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(titleType: .members)
			return cell
		case .members:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.TrackCell) as? MLElementTableViewCell else {
				return UITableViewCell()
			}

			cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
			cell.setupCell(elementName: member?.name, elementType: .members)
			return cell
		}
	}
}

class MLArtistDetailViewController		: UIViewController {

	// ========================================================
	// MARK: - Outlets
	// ========================================================

	@IBOutlet private weak var tableView	: UITableView?

	// ========================================================
	// MARK: - Public properties
	// ========================================================

	var artist 							: Artist?

	// ========================================================
	// MARK: - Private properties
	// ========================================================

	private var members					: [Member]?
	private var artistDescription		: String?

	// ========================================================
	// MARK: - Private properties
	// ========================================================

	private var artistResourceURLString	: String?

	// ========================================================
	// MARK: - View life cycle
	// ========================================================

	override func viewDidLoad() {
		super.viewDidLoad()

		self.title = self.artist?.name
		self.setupTableView()
		self.fetchResourceURLFromArtist()
	}

	// ========================================================
	// MARK: - Private Fetch Methods
	// ========================================================

	private func fetchResourceURLFromArtist() {

		guard let artistId = self.artist?.id else {
			return
		}

		APIManager.fetchImageResourceURLStringFromArtist(artistId: artistId) { [weak self] (resourceURL: String, error: Error?) in

			self?.artistResourceURLString = resourceURL
			DispatchQueue.main.async {
				self?.tableView?.reloadData()
			}

			self?.fetchDescriptionAndMembers(artistId: artistId)
		}
	}

	private func fetchDescriptionAndMembers(artistId: Int) {

		APIManager.fetchDescriptionAndMembers(with: artistId) { [weak self] (artistDescription: String?, members: [Member]?, error: Error?) in
			self?.artistDescription = artistDescription
			self?.members = members

			DispatchQueue.main.async {
				self?.tableView?.reloadData()
			}
		}
	}

	// ========================================================
	// MARK: - Private Setup Methods
	// ========================================================

	private func setupTableView() {
		self.tableView?.tableFooterView = UIView()
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.AlbumThumbnailCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.AlbumThumbnailCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.ElementDescriptionCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.ElementDescriptionCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.AlbumTracksCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.AlbumTracksCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.TrackCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.TrackCell)

		self.tableView?.rowHeight = UITableViewAutomaticDimension
	}
}

// ========================================================
// MARK: - Table View Methods
// ========================================================

extension MLArtistDetailViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		// Cause the number of members is unknown at the beginning, we wait for the server answer to show the correct number of them
		// if there is no members, do not show the last two cells (tracks information)
		guard  let members = self.members else {
			return (ArtistDetail.allValues().count - 2)
		}

		// One member is already considered in enum, so we need to add the total of tracks minus 1
		return (ArtistDetail.allValues().count + (members.count - 1))
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		guard let artistDetailCase = ArtistDetail(rawValue: indexPath.row) else {
			let member = self.members?[indexPath.row - (AlbumDetail.allValues().count - 2)]
			return ArtistDetail.members.cellForAlbumDetail(tableView: tableView, artistResourceURLString: self.artistResourceURLString, artistDescription: self.artistDescription, member: member)
		}

		let member = ((artistDetailCase == .members) ? self.members?[indexPath.row - (ArtistDetail.allValues().count - 1)] : nil)
		return artistDetailCase.cellForAlbumDetail(tableView: tableView, artistResourceURLString: self.artistResourceURLString, artistDescription: self.artistDescription, member: member)
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		guard let artistDetailCase = ArtistDetail(rawValue: indexPath.row) else {
			return ArtistDetail.members.cellHeight
		}

		return ((artistDetailCase == .description) ? UITableViewAutomaticDimension : artistDetailCase.cellHeight)
	}
}
