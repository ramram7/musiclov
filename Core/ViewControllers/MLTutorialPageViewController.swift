//
//  MLTutorialPageViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 07.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

// ========================================================
// MARK - Tutorial Enum
//
// Enum including all tutorial cases
// Every case contains three variables:
//     * Title of tutorial section
//     * Image for tutorial section
//     * Description text fot tutorial section
//
// ========================================================

enum Tutorial: Int {

	case welcome = 0
	case search
	case favourites

	static var allValues: [Tutorial] {
		return [.welcome, .search, .favourites]
	}

	var title: String {
		switch self {
		case .welcome		: return NSLocalizedString("WelcomeTutorial.Welcome.Title", comment: "")
		case .search		: return NSLocalizedString("WelcomeTutorial.Search.Title", comment: "")
		case .favourites	: return NSLocalizedString("WelcomeTutorial.Favourites.Title", comment: "")
		}
	}

	var image: UIImage? {
		var imageName: String
		switch self {
		case .welcome		: imageName = "ic_welcome"
		case .search		: imageName = "ic_search"
		case .favourites	: imageName = "ic_favourite"
		}

		return UIImage(named: imageName)
	}

	var description: String {
		switch self {
		case .welcome		: return NSLocalizedString("WelcomeTutorial.Welcome.Description", comment: "")
		case .search		: return NSLocalizedString("WelcomeTutorial.Search.Description", comment: "")
		case .favourites	: return NSLocalizedString("WelcomeTutorial.Favourites.Description", comment: "")
		}
	}
}

class MLTutorialPageViewController		: UIPageViewController {

	// ========================================================
	// MARK: - Computed variables
	// ========================================================

	var tutorialViewControllerInstance	: MLTutorialViewController? {

		let storyboard = UIStoryboard(name: MLConstants.StoryboardName.WelcomeTutorial, bundle: nil)
		return storyboard.instantiateViewController(withIdentifier: MLConstants.StoryboardId.WelcomeViewController) as? MLTutorialViewController
	}

	// ========================================================
	// MARK: - View Life Cycle
	// ========================================================

    override func viewDidLoad() {
        super.viewDidLoad()

		self.initialPageControllerSetup()
    }

	// ========================================================
	// MARK: - Page Controller configuration
	// ========================================================

	private func initialPageControllerSetup() {
		self.dataSource = self
		if let firstViewController = self.tutorialViewControllerInstance {
			firstViewController.tutorialType = .welcome
			self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
		}
	}
}

// ========================================================
// MARK: - Page Controller Data Source Protocol Implementation
// ========================================================

extension MLTutorialPageViewController: UIPageViewControllerDataSource {

	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		guard let currentViewControllerIndex = self.indexFromCurrentTutorialViewController(viewController: viewController) else {
			return nil
		}

		let previousIndex = (currentViewControllerIndex - 1)

		guard
			(previousIndex >= 0),
			(Tutorial.allValues.count > previousIndex),
			let tutorialViewController = self.tutorialViewControllerInstance else {
				return nil
		}

		tutorialViewController.tutorialType = Tutorial.allValues[previousIndex]
		return tutorialViewController
	}

	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

		guard let currentViewControllerIndex = self.indexFromCurrentTutorialViewController(viewController: viewController) else {
			return nil
		}

		let nextIndex = (currentViewControllerIndex + 1)

		guard
			(Tutorial.allValues.count != nextIndex),
			(Tutorial.allValues.count > nextIndex),
			let tutorialViewController = self.tutorialViewControllerInstance else {
				return nil
		}

		tutorialViewController.tutorialType = Tutorial.allValues[nextIndex]
		return tutorialViewController
	}

	private func indexFromCurrentTutorialViewController(viewController: UIViewController) -> Int? {

		guard
			let viewController = viewController as? MLTutorialViewController,
			let currentViewControllerIndex = Tutorial.allValues.index(of: viewController.tutorialType) else {
				return nil
		}

		return currentViewControllerIndex
	}

	func presentationCount(for pageViewController: UIPageViewController) -> Int {
		return Tutorial.allValues.count
	}

	func presentationIndex(for pageViewController: UIPageViewController) -> Int {
		guard
			let firstViewController = self.viewControllers?.first as? MLTutorialViewController,
			let firstViewControllerIndex = Tutorial.allValues.index(of: firstViewController.tutorialType) else {
				return 0
		}

		return firstViewControllerIndex
	}
}
