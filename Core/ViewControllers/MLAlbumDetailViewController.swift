//
//  MLAlbumDetailViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 09.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

// ========================================================
// MARK - AlbumDetail Enum
//
// Enum including all lbum detail cases
// Every case contains three variables:
//     * thumbnail - Thumbnail from album
//     * artistName - Artist Name
//     * albumDetails - Details from album
//     * trackTitle
//     * track name
//
// ========================================================
enum AlbumDetail: Int {
	case thumbnail = 0
	case artistName
	case albumDetails
	case trackTitle
	case tracks

	static func allValues() -> [AlbumDetail] {
		return [.thumbnail, .artistName, .albumDetails, .trackTitle, .tracks]
	}

	var cellHeight: CGFloat {

		switch self {
		case .artistName,
			 .trackTitle,
			 .tracks		: return 44
		case .thumbnail		: return 250
		case .albumDetails	: return 150
		}
	}

	func cellForAlbumDetail(album: Album, tableView: UITableView, track: Track? = nil, resourceURLString: String? = nil) -> UITableViewCell {

		switch self {
		case .thumbnail		:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.AlbumThumbnailCell) as? MLAlbumThumbTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(resourceURLString: resourceURLString)
			return cell

		case .artistName	:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.ArtistNameCell) as? MLArtistTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(album: album)
			return cell

		case .albumDetails	:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.AlbumDetailsCell) as? MLAlbumDetailsTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(album: album)
			return cell

		case .trackTitle	:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.AlbumTracksCell) as? MLTitleTableViewCell else {
				return UITableViewCell()
			}

			cell.setupCell(titleType: .tracks)
			return cell
		case .tracks		:
			guard
				let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.TrackCell) as? MLElementTableViewCell,
				let track = track else {
					return UITableViewCell()
			}

			cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
			cell.setupCell(elementName: track.title, elementType: .tracks)
			return cell
		}
	}
}

class MLAlbumDetailViewController			: UIViewController {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var tableView	: UITableView?

	// ========================================================
	// MARK: - Public properties
	// ========================================================

	var album 								: Album?

	// ========================================================
	// MARK: - Private properties
	// ========================================================

	private var tracks						: [Track]?
	private var artist						: Artist?
	private var resourceURLFromAlbum		: String?

	// ========================================================
	// MARK: - View Life Cycle
	// ========================================================

	override func viewDidLoad() {
        super.viewDidLoad()

		self.setupTableView()

		let titleComponents = self.album?.title.split(separator: "-")
		self.title = String(titleComponents?.last ?? "")
		self.fetchResourceURLFromAlbum()
    }

	// ========================================================
	// MARK: - Private Fetch Methods
	// ========================================================

	private func fetchResourceURLFromAlbum() {
		guard let albumId = self.album?.id else {
			return
		}

		APIManager.fetchImageResourceURLStringFromAlbum(albumId: albumId) { [weak self] (resourceURLString: String, error: Error?) in
			self?.resourceURLFromAlbum = resourceURLString
			DispatchQueue.main.async {
				self?.tableView?.reloadData()
			}
			self?.fetchTracksAndArtistForAlbum(albumId: albumId)
		}
	}

	private func fetchTracksAndArtistForAlbum(albumId: Int) {

		APIManager.fetchTracksAndArtistForAlbum(with: albumId) { [weak self] (tracks: [Track]?, artist: Artist?, error: Error?) in

			self?.tracks = tracks
			self?.artist = artist
			DispatchQueue.main.async {
				self?.tableView?.reloadData()
			}
		}
	}

	// ========================================================
	// MARK: - Private Setup Methods
	// ========================================================

	private func setupTableView() {
		self.tableView?.tableFooterView = UIView()
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.AlbumThumbnailCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.AlbumThumbnailCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.ArtistNameCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.ArtistNameCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.AlbumDetailsCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.AlbumDetailsCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.AlbumTracksCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.AlbumTracksCell)
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.TrackCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.TrackCell)
	}
}

// ========================================================
// MARK: - Navigation Methods
// ========================================================

extension MLAlbumDetailViewController {

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		guard
			(segue.identifier == MLConstants.Segues.ArtistDetailViewController),
			let artistViewController = segue.destination as? MLArtistDetailViewController else {
				return
		}

		artistViewController.artist = self.artist
	}
}

// ========================================================
// MARK: - TableView Protocols methods
// ========================================================

extension MLAlbumDetailViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		// Cause the number of tracks is unknown at the beginning, we wait for the server answer to show the correct number of them
		// if there is no tracks, do not show the last two cells (tracks information)
		guard  let tracks = self.tracks else {
			return (AlbumDetail.allValues().count - 2)
		}

		// One track is already considered in enum, so we need to add the total of tracks minus 1
		return (AlbumDetail.allValues().count + (tracks.count - 1))
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		guard let album = self.album else {
			return UITableViewCell()
		}

		guard
			let albumDetailCase = AlbumDetail(rawValue: indexPath.row),
			let tracksCount = self.tracks?.count,
			(indexPath.row - (AlbumDetail.allValues().count - 1) < tracksCount) else {
				let track = self.tracks?[indexPath.row - (AlbumDetail.allValues().count - 1)]
				return AlbumDetail.tracks.cellForAlbumDetail(album: album, tableView: tableView, track: track, resourceURLString: self.resourceURLFromAlbum)
		}

		let isTrackBeingShown = (albumDetailCase == .tracks && (indexPath.row - (AlbumDetail.allValues().count - 1) < tracksCount))
		let track = ((isTrackBeingShown == true) ? self.tracks?[indexPath.row - (AlbumDetail.allValues().count - 1)] : nil)
		return albumDetailCase.cellForAlbumDetail(album: album, tableView: tableView, track: track, resourceURLString: self.resourceURLFromAlbum)
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)

		guard
			let albumDetailCase = AlbumDetail(rawValue: indexPath.row),
			(albumDetailCase == .artistName) else {
				return
		}

		self.performSegue(withIdentifier: MLConstants.Segues.ArtistDetailViewController, sender: nil)
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		guard let albumDetailCase = AlbumDetail(rawValue: indexPath.row) else {
			return AlbumDetail.tracks.cellHeight
		}

		return albumDetailCase.cellHeight
	}
}
