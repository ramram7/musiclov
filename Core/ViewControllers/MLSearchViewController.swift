//
//  MLSearchViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 07.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLSearchViewController				: UIViewController {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var tableView	: UITableView?

	// ========================================================
	// MARK: - Public properties
	// ========================================================

	var albums								= [Album]()
	var searchController 					= UISearchController(searchResultsController: nil)
	var lastSearchAlbumString				: String?

	// ========================================================
	// MARK: - View Life cycle
	// ========================================================

    override func viewDidLoad() {
        super.viewDidLoad()

		self.setupSearchController()
		self.setupTableView()

		self.title = NSLocalizedString("SearchView.Title", comment: "")
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		self.showTutorialViewIfNeeded()
	}

	// ========================================================
	// MARK: - Private Functions
	// ========================================================

	private func showTutorialViewIfNeeded() {

		let standardUserDefaults = UserDefaults.standard
		guard (standardUserDefaults.bool(forKey: MLConstants.UserDefaults.AppAlreadyLaunchedForFirstTime) == false) else {
			return
		}

		standardUserDefaults.set(true, forKey: MLConstants.UserDefaults.AppAlreadyLaunchedForFirstTime)
		self.performSegue(withIdentifier: MLConstants.Segues.WelcomeTutorial, sender: nil)
	}


	// ========================================================
	// MARK: - Private Setup Functions
	// ========================================================

	private func setupSearchController() {

		self.searchController.searchBar.placeholder = NSLocalizedString("SearchView.SearchForAlbum", comment: "")
		self.searchController.searchBar.delegate = self
		self.searchController.delegate = self
		self.navigationItem.searchController?.hidesNavigationBarDuringPresentation = false
		self.navigationItem.searchController = self.searchController
		self.navigationItem.hidesSearchBarWhenScrolling = false
		self.definesPresentationContext = true
	}

	private func setupTableView() {
		self.tableView?.tableFooterView = UIView()
		let emptySearchView = MLEmptySearchView.instanceFromNib(with: .noSearch)
		self.tableView?.backgroundView = emptySearchView
		self.tableView?.register(UINib(nibName: MLConstants.CellReuseId.AlbumCell, bundle: self.nibBundle), forCellReuseIdentifier: MLConstants.CellReuseId.AlbumCell)
	}

	// ========================================================
	// MARK: - Private Fetch Functions
	// ========================================================

	private func fetchAlbums(with albumTitle: String) {

		APIManager.fetchAlbums(title: albumTitle) { [weak self] (albums: [Album]?, error: Error?) in
			guard let albums = albums else {
				self?.albums.removeAll()
				self?.tableView?.reloadData()
				return
			}

			self?.albums = albums
			DispatchQueue.main.async {
				self?.tableView?.reloadData()

				if (self?.albums.isEmpty == true) {
					let emptySearchView = MLEmptySearchView.instanceFromNib(with: .noResults)
					self?.tableView?.backgroundView = emptySearchView

				} else {
					self?.tableView?.backgroundView = nil
				}

				self?.searchController.isActive = false
			}
		}
	}
}

// ========================================================
// MARK: - TableView Protocols Implementation
// ========================================================

extension MLSearchViewController {

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard
			(segue.identifier == MLConstants.Segues.AlbumArtistDetail),
			let albumDetailViewController = segue.destination as? MLAlbumDetailViewController,
			let album = sender as? Album else {
				return
		}

		albumDetailViewController.album = album
	}

}

// ========================================================
// MARK: - TableView Protocols Implementation
// ========================================================

extension MLSearchViewController: UITableViewDataSource, UITableViewDelegate {

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: MLConstants.CellReuseId.AlbumCell) as? MLAlbumTableViewCell else {
			return UITableViewCell()
		}

		cell.setupCell(album: self.albums[indexPath.row])
		return cell
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.albums.count
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 70
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)

		guard (indexPath.row < self.albums.count) else {
			return
		}

		self.performSegue(withIdentifier: MLConstants.Segues.AlbumArtistDetail, sender: self.albums[indexPath.row])
	}
}

// ========================================================
// MARK: - UISearchBarDelegate Protocol Implementation
// ========================================================

extension MLSearchViewController: UISearchBarDelegate {

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		guard
			let text = searchBar.text,
			(text.isEmpty == false),
			(text.trimmingCharacters(in: .whitespaces).isEmpty == false) else {
				return
		}

		self.fetchAlbums(with: text)
	}
}

// ========================================================
// MARK: - UISearchControllerDelegate Protocol Implementation
// ========================================================

extension MLSearchViewController: UISearchControllerDelegate {

	func willDismissSearchController(_ searchController: UISearchController) {
		guard (searchController.searchBar.text?.isEmpty == false && searchController.searchBar.text?.trimmingCharacters(in: .whitespaces).isEmpty == false) else {
			self.lastSearchAlbumString = nil
			return
		}

		self.lastSearchAlbumString = searchController.searchBar.text
	}

	func didDismissSearchController(_ searchController: UISearchController) {
		guard let lastSearchAlbumString = self.lastSearchAlbumString else {
			return
		}

		searchController.searchBar.text = lastSearchAlbumString
	}
}
