//
//  MLTutorialViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 07.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLTutorialViewController					: UIViewController {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var titleLabel		: UILabel?
	@IBOutlet private weak var imageView		: UIImageView?
	@IBOutlet private weak var descriptionLabel	: UILabel?
	@IBOutlet private weak var letsGoButton		: UIButton?

	// ========================================================
	// MARK: - Public Properties
	// ========================================================

	var tutorialType							: Tutorial = .welcome

	// ========================================================
	// MARK: - View Life cycle
	// ========================================================

    override func viewDidLoad() {
        super.viewDidLoad()

		self.titleLabel?.text = self.tutorialType.title
		self.imageView?.image = self.tutorialType.image
		self.descriptionLabel?.text = self.tutorialType.description
		self.letsGoButton?.setTitle(NSLocalizedString("WelcomeTutorial.LetsGo", comment: ""), for: .normal)
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		UIView.animate(withDuration: 0.3) {
			self.letsGoButton?.alpha = ((self.tutorialType == .favourites) ? 1.0 : 0)
		}
	}
}

// ========================================================
// MARK: - Extension with action methods
// ========================================================

extension MLTutorialViewController {

	@IBAction func closeTutorialPressed(_ sender: Any) {
		self.parent?.dismiss(animated: true, completion: nil)
	}

}
