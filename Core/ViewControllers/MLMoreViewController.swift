//
//  MLMoreViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 12.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

enum MoreCase: Int {

	case generalOverview = 0
	case todos
	case icons

	static func allValues() -> [MoreCase] {
		return [.generalOverview, .todos, .icons]
	}

	var title: String {
		switch self {
		case .generalOverview	: return "Overview"
		case .todos				: return "ToDos"
		case .icons				: return "Icon credits"
		}
	}

	var segueToPerform: String {
		switch self {
		case .generalOverview	: return "toGeneralOverview"
		case .todos				: return "toToDos"
		case .icons				: return "toIcons"
		}
	}
}

class MLMoreViewController: UIViewController {

	// ========================================================
	// MARK: - Outlets
	// ========================================================

	@IBOutlet private weak var tableView	: UITableView?

	// ========================================================
	// MARK: - Outlets
	// ========================================================

	override func viewDidLoad() {
		super.viewDidLoad()

		self.tableView?.tableFooterView = UIView()
	}
}

extension MLMoreViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return MoreCase.allValues().count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") else {
			return UITableViewCell()
		}

		cell.textLabel?.numberOfLines = 0
		cell.textLabel?.text = MoreCase(rawValue: indexPath.row)?.title
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)

		guard let segueIdentifier = MoreCase.init(rawValue: indexPath.row)?.segueToPerform else {
			return
		}

		self.performSegue(withIdentifier: segueIdentifier, sender: nil)
	}
}
