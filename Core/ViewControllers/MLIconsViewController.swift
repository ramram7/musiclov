//
//  MLIconsViewController.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 12.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLIconsViewController					: UIViewController {

	@IBOutlet private weak var tableView	: UITableView?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.tableView?.tableFooterView = UIView()
	}
}

extension MLIconsViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") else {
			return UITableViewCell()
		}

		cell.textLabel?.numberOfLines = 0
		cell.selectionStyle = .none
		cell.textLabel?.text = """
		I really believe that when someone uses code/icons/ideas from someone else, this needs to be mentioned in one or another way.

		In this case, I used an amazing site full of awesome icons for free, the only condition is to give credit to the site and author.
		So here we go:

			♣️ App Icon: Freepik -> http://www.freepik.com
			♣️ Welcome tutorial Star: Freepik -> http://www.freepik.com
			♣️ In Progress icon: Dimi Kazak ->https://www.flaticon.com/authors/dimi-kazak
			♣️ Singer: Freepik -> http://www.freepik.com
			♣️ Music track: Smashicons -> https://www.flaticon.com/authors/smashicons
			♣️ No results: Freepik -> http://www.freepik.com
			♣️ Search: DinosoftLabs -> https://www.flaticon.com/authors/dinosoftlabs
			♣️ Welcome: Roundicons -> https://www.flaticon.com/authors/roundicons

		All the icons were found in https://www.flaticon.com/ ❤️
		"""
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
}


