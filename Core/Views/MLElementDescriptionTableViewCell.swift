//
//  MLElementDescriptionTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 11.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLElementDescriptionTableViewCell				: UITableViewCell {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var descriptionLabel		: UILabel?

	// ========================================================
	// MARK: - Setup Methods
	// ========================================================

	func setupCell(description: String?) {

		self.selectionStyle = .none
		self.descriptionLabel?.text = description
	}

}
