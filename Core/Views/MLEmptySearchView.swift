//
//  MLEmptySearchView.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 08.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

enum EmptyModeStates {
	case noSearch
	case noResults

	var image: UIImage? {
		var imageName	: String
		switch self {
		case .noSearch	: imageName = "ic_search"
		case .noResults	: imageName = "ic_noResults"
		}

		return UIImage(named: imageName)
	}

	var description		: String {
		switch self {
		case .noSearch	: return NSLocalizedString("SearchView.SearchForAlbum", comment: "")
		case .noResults	: return NSLocalizedString("SearchView.NoResultsFound", comment: "")
		}
	}
}

class MLEmptySearchView								: UIView {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var imageView			: UIImageView?
	@IBOutlet private weak var descriptionLabel		: UILabel?

	// ========================================================
	// MARK: - Public properties
	// ========================================================

	var emptyState									: EmptyModeStates?

	class func instanceFromNib(with mode: EmptyModeStates) -> MLEmptySearchView? {
		let emptySearchView = (Bundle.main.loadNibNamed(MLConstants.NibNames.EmptySearchView, owner: self, options: nil)?.first as? MLEmptySearchView)
		emptySearchView?.emptyState = mode
		emptySearchView?.setupEmptySearchView()
		return emptySearchView
	}

	private func setupEmptySearchView() {
		self.imageView?.image = self.emptyState?.image
		self.descriptionLabel?.text = self.emptyState?.description
	}
}
