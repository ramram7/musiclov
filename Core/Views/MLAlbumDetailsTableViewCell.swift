//
//  MLAlbumDetailsTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 10.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLAlbumDetailsTableViewCell					: UITableViewCell {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var formatsTitleLabel	: UILabel?
	@IBOutlet private weak var formatsLabel			: UILabel?
	@IBOutlet private weak var releasedTitleLabel	: UILabel?
	@IBOutlet private weak var releasedLabel		: UILabel?
	@IBOutlet private weak var genreTitleLabel		: UILabel?
	@IBOutlet private weak var genreLabel			: UILabel?
	@IBOutlet private weak var styleTitleLabel		: UILabel?
	@IBOutlet private weak var styleLabel			: UILabel?

	// ========================================================
	// MARK: - Setup methods
	// ========================================================

	func setupCell(album: Album) {
		self.selectionStyle = .none

		self.setTitleLabels()
		self.setAlbumInformation(album: album)
	}

	private func setTitleLabels() {
		self.formatsTitleLabel?.text = NSLocalizedString("Formats", comment: "")
		self.formatsTitleLabel?.layer.cornerRadius = 3
		self.releasedTitleLabel?.text = NSLocalizedString("Released", comment: "")
		self.releasedTitleLabel?.layer.cornerRadius = 3
		self.genreTitleLabel?.text = NSLocalizedString("Genre", comment: "")
		self.genreTitleLabel?.layer.cornerRadius = 3
		self.styleTitleLabel?.text = NSLocalizedString("Style", comment: "")
		self.styleTitleLabel?.layer.cornerRadius = 3
	}

	override func layoutSubviews() {
		super.layoutSubviews()

		self.formatsTitleLabel?.layer.cornerRadius = 10
		self.formatsTitleLabel?.layer.masksToBounds = true
		self.releasedTitleLabel?.layer.cornerRadius = 10
		self.releasedTitleLabel?.layer.masksToBounds = true
		self.genreTitleLabel?.layer.cornerRadius = 10
		self.genreTitleLabel?.layer.masksToBounds = true
		self.styleTitleLabel?.layer.cornerRadius = 10
		self.styleTitleLabel?.layer.masksToBounds = true
	}

	private func setAlbumInformation(album: Album) {

		self.formatsLabel?.text = album.formats.joined(separator: ", ")
		self.releasedLabel?.text = album.year
		self.genreLabel?.text = album.genre.joined(separator: ", ")
		self.styleLabel?.text = album.style.joined(separator: ", ")
	}
}
