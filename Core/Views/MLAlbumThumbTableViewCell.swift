//
//  MLAlbumThumbTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 09.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit
import WebKit

class MLAlbumThumbTableViewCell				: UITableViewCell {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet weak var albumThumbWebView	: WKWebView?

	func setupCell(resourceURLString: String?) {

		self.selectionStyle = .none
		self.setupThumbnailWebView(resourceURLString: resourceURLString)
	}

	private func setupThumbnailWebView(resourceURLString: String?) {

		self.albumThumbWebView?.load(URLRequest(url: URL(string:"about:blank")!))
		self.albumThumbWebView?.contentMode = .center

		guard
			let resourceURLString = resourceURLString,
			let url = URL(string: resourceURLString) else {
				return
		}

		self.albumThumbWebView?.alpha = 0.0
		self.albumThumbWebView?.navigationDelegate = self
		let urlRequest = URLRequest(url: url)
		self.albumThumbWebView?.load(urlRequest)
	}
}

// ========================================================
// MARK: - WKNavigationDelegate Protocol Implementation
// ========================================================

extension MLAlbumThumbTableViewCell: WKNavigationDelegate {

	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

		UIView.animate(withDuration: 0.3, animations: {
			self.albumThumbWebView?.alpha = 1.0

		})
	}
}
