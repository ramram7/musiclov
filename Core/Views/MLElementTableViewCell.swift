//
//  MLElementTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 10.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLElementTableViewCell: UITableViewCell {

	@IBOutlet private weak var trackLabel		: UILabel?
	@IBOutlet private weak var elementImageView	: UIImageView?

	func setupCell(elementName: String?, elementType: ElementType) {

		self.selectionStyle = .none
		self.trackLabel?.text = elementName
		self.imageView?.image = elementType.image
	}
}
