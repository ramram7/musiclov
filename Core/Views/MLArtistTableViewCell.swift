//
//  MLArtistTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 10.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

class MLArtistTableViewCell						: UITableViewCell {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var artistNameLabel	: UILabel?

	// ========================================================
	// MARK: - Setup Methods
	// ========================================================

	func setupCell(album: Album) {

		self.selectionStyle = .none
		let titleComponents = album.title.split(separator: "-")

		guard let artistName = titleComponents.first else {
			return
		}

		self.artistNameLabel?.text = String(artistName)
	}
}
