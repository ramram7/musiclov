//
//  MLTitleTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 10.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

enum ElementType {
	case tracks
	case members

	var title: String {
		switch self {
		case .tracks	: return NSLocalizedString("Tracks", comment: "")
		case .members	: return NSLocalizedString("Members", comment: "")
		}
	}

	var image: UIImage? {
		let imageName: String
		switch self {
		case .tracks	: imageName = "ic_track"
		case .members	: imageName = "ic_member"
		}

		return UIImage(named: imageName)
	}
}

class MLTitleTableViewCell							: UITableViewCell {

	@IBOutlet private weak var tracksTitleLabel		: UILabel?

	func setupCell(titleType: ElementType) {

		self.selectionStyle = .none
		self.tracksTitleLabel?.text = (titleType.title)
	}
}
