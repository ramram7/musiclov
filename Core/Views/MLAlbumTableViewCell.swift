//
//  MLAlbumTableViewCell.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 09.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit
import WebKit

class MLAlbumTableViewCell						: UITableViewCell {

	// ========================================================
	// MARK: - Private Outlets
	// ========================================================

	@IBOutlet private weak var thumbnailWebView	: WKWebView?
	@IBOutlet private weak var albumTitleLabel	: UILabel?
	@IBOutlet private weak var artistLabel		: UILabel?

	// ========================================================
	// MARK: - Private properties
	// ========================================================

	private var album 									: Album?

	// ========================================================
	// MARK: - Setup Methods
	// ========================================================

	func setupCell(album: Album) {

		self.album = album
		self.setupAlbumAndArtistName()
		self.setupThumbnailWebView()
		self.accessoryType = .disclosureIndicator
	}

	private func setupAlbumAndArtistName() {

		guard let album = self.album else {
			return
		}

		let albumAndArtistElements = album.title.split(separator: "-")

		if let albumTitle = albumAndArtistElements.last {
			self.albumTitleLabel?.text = String(describing: albumTitle)
		}

		if let artist = albumAndArtistElements.first {
			self.artistLabel?.text = String(describing: artist)
		}
	}

	private func setupThumbnailWebView() {

		self.thumbnailWebView?.load(URLRequest(url: URL(string:"about:blank")!))

		guard
			let album = self.album,
			let url = URL(string: album.thumb) else {
				return
		}

		self.thumbnailWebView?.alpha = 0.0
		self.thumbnailWebView?.navigationDelegate = self
		let urlRequest = URLRequest(url: url)
		self.thumbnailWebView?.load(urlRequest)

	}
}

// ========================================================
// MARK: - WKNavigationDelegate Protocol Implementation
// ========================================================

extension MLAlbumTableViewCell: WKNavigationDelegate {

	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

		if (self.album?.thumb == webView.url?.absoluteString) {
			UIView.animate(withDuration: 0.3, animations: {
				self.thumbnailWebView?.alpha = 1.0

			})
		}
	}
}
