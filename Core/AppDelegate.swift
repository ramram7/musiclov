//
//  AppDelegate.swift
//  MusicLov
//
//  Created by Ramiro Ramirez on 07.03.18.
//  Copyright © 2018 ramram. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
}

